# Emoji Tetra Questions and Answers

## General

### What is all this?

[Emoji Tetra](https://twitter.com/EmojiTetra) is a twitter bot that uses twitter polls to make communal decisions for a falling block 
game.

### Who made this?

[Joe Sondow](https://twitter.com/JoeSondow) wrote most of the code.

[Bradley Momberger](https://twitter.com/air_hadoken) and [Stefan Bohacek](https://twitter.com/fourtonfish) 
discovered and shared the method for performing read/write access to Twitter polls.

Some improvements to the visual design and game play are thanks to many clever people who chat with 
me when I [livestream my coding sessions on Twitch](https://twitch.tv/JoeSondow).

The game play is inspired by Tetris, invented by Alexey Pajitnov, with trademark and copyright owned
by The Tetris Company.

### Why did you make this?

I felt like it. If you'd like a longer answer, [here's a video of a talk I gave about my Twitter 
bots](https://twitter.com/JoeSondow/status/954424471510831104).

### My question isn't answered here. What do I do?

Live with the crushing disappointment, or tweet your question to me at 
[@JoeSondow](https://twitter.com/JoeSondow) and I'll do my best to address it.

### Why are the Emoji Tetra tweets threaded?

People conventionally expect a game like this to be animated, but that is not an option in the
Twitter medium. The closest I can get to connecting each moment in time to the game's past and 
future states is to thread the tweets. This allows people who see a single EmojiTetra tweet in their
timeline to click on it and immediately see its context in the running game, without having to 
navigate elsewhere first. If EmojiTetra tweets were not threaded, then on the one hand it would be
easier for people to read player discussion threads below a single tweet that occurred hours or days
ago, but on the other hand it would be harder to see the current game state in context with all the 
prior moves. Threading also collapses dozens of tweets into just three tweets on a follower's 
timeline. I'm not comfortable spreading out this many frequent, unthreaded tweets across so many 
people's timeline's without the collapsing benefits of threads. It's a trade off either way, and I 
(Joe Sondow) have decided that it is better for the overall player experience for the tweets to be 
threaded.

### Can't players just navigate to the EmojiTetra profile screen to read the tweets in order instead of threading?

They could but most players play when a tweet appears in their feed, rather than having to go to the
EmojiTetra profile first.

### It's hard to find the discussion tweets because of the threading. Could you retweet the replies that get the most Likes?

I've thought about this. It would be easily games and abused, so it would require careful manual 
curation of trustworthy players. It's not as simple and safe as it may sound at first glance. It's
not a high priority for me at the moment.

## Rules

### Is Joe controlling which shape spawns next?

Nope. The pseudorandom hand of digital fate handles that.

### What are the rotation rules?
````
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️🍎◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍎◻️◻️
◻️🍎🍎◻️◻️➖◻️🍎🍎◻️◻️➖◻️🍎🍎◻️◻️➖◻️🍎🍎◻️◻️
◻️◻️🍎🍎◻️➖◻️🍎◻️◻️◻️➖◻️◻️🍎🍎◻️➖◻️🍎◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️
◻️🍑🍑🍑🍑➖◻️◻️🍑◻️◻️➖◻️🍑🍑🍑🍑➖◻️◻️🍑◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍑◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️🍋◻️◻️➖◻️◻️🍋◻️◻️➖◻️◻️🍋◻️◻️
◻️🍋🍋🍋◻️➖◻️◻️🍋🍋◻️➖◻️🍋🍋🍋◻️➖◻️🍋🍋◻️◻️
◻️◻️🍋◻️◻️➖◻️◻️🍋◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍋◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️🍏🍏◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍏◻️◻️
◻️🍏🍏🍏◻️➖◻️🍏◻️◻️◻️➖◻️🍏◻️◻️◻️➖◻️◻️🍏◻️◻️
◻️◻️◻️🍏◻️➖◻️🍏◻️◻️◻️➖◻️🍏🍏🍏◻️➖◻️🍏🍏◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️
◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️➖◻️◻️🍇🍇◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️🍉◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️🍉🍉◻️◻️
◻️🍉🍉🍉◻️➖◻️🍉◻️◻️◻️➖◻️◻️◻️🍉◻️➖◻️◻️🍉◻️◻️
◻️🍉◻️◻️◻️➖◻️🍉🍉◻️◻️➖◻️🍉🍉🍉◻️➖◻️◻️🍉◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
◻️◻️◻️◻️◻️➖◻️◻️🍓◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️🍓◻️◻️
◻️◻️🍓🍓◻️➖◻️◻️🍓🍓◻️➖◻️◻️🍓🍓◻️➖◻️◻️🍓🍓◻️
◻️🍓🍓◻️◻️➖◻️◻️◻️🍓◻️➖◻️🍓🍓◻️◻️➖◻️◻️◻️🍓◻️
◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️➖◻️◻️◻️◻️◻️
````

### Why can't we rotate when a piece is at the side of the grid?

I've implemented ceiling kicks (implicit Down moves when rotating against the solid ceiling) but not
wall kicks, because they aren't as important for speeding up the game.

### Are there wall kicks or ceiling kicks, to allow rotation when a piece is near a grid edge?

There are ceiling kicks. There are no wall kicks so far.

### How are ties broken?

Down/Stop wins ties that include Down/Stop. Other ties are broken by random choice.

### Why is the Plummet option sometimes missing?

Twitter polls only allow up to four choices. When a piece is falling through open space, we need all
four slots for Left Right Rotate Down. When the ability to move or rotate is blocked, one or more of
those options becomes unavailable, making room for the Plummet option.

### Why is there sometimes a "Left or Right" poll option?

Twitter polls only allow four options. In practice, players rarely elect Left or Right immediately
after a Down move. Therefore, to reduce the number of polls per piece, immediately after a Down move
the game combines "Left" and "Right" into a single poll option labeled "Left or Right" in order to
make room in the poll for the very useful "Plummet" option. In the rare circumstance where players 
elect the "Left or Right" option, the next poll shows the piece has not moved yet, and the poll 
consists only of the options "Left" and "Right".

When players elect any choice other than "Down", the "Left or Right" option will not be in the
next poll.

### Why do pieces sometimes fall faster and sometimes slower?

To speed up the game, when there are multiple blank lines below the falling piece, the Down choice
causes the piece to descend half of those blank lines, rounded up.

### Why does the piece sometimes turn black when it's about to land?

Most of the time you can tell which piece you are controlling by looking at which tiles have empty
space below them. When a piece has reached the bottom of its fall, there are still times when you
want to slide it into place underneath other tiles before locking the piece down. In the context of
a non-animated tweet, we need some way to differentiate the currently moving piece from its
surroundings. Thus, the black tiles in that situation.

### Why not make the falling piece black all the time for consistency?

It wouldn't be as pretty.

### How does the score board work?

For a single line cleared, players earn 100 points.

For two lines cleared in one move, players earn 125 points per row, for a total of 250 points.

For three lines cleared in one move, players earn 175 points per row, for a total of 525 points.

For four lines cleared in one move, players earn 250 points per row, for a total of 1000 points.

If players 100% clear the entire grid, they earn an additional 1000 points.

### Is there a bonus for clearing the entire grid?

Yes, 1000 bonus points.

### Are there scoring bonuses for performing T-spins?

No.

### Are there special cases implemented for how T-spins work in tight spaces?

No.

### Is there a place to see the all-time high score?

Yes, in the account bio.

### Can we see what shape is coming next?

Yes, at the top of each tweet.

### If Down and Plummet lose a poll because votes are split between them, do Plummet's votes go to Down?

Not yet, but [I intend to implement that](https://gitlab.com/JoeSondow/emojitetra/issues/31) at some point. 

### Why does the piece sometimes seem to Plummet when we choose Down?

As the piece falls, each time it cannot do a Left/Right/Rotate move, that turn gets skipped and
rolled up into a bunch of Down moves all at once, often ending with a Stop. This is to save time and
also because Twitter polls must have 2-4 choices, never just 1 choice. 

Also, there are rare cases a bottleneck formation causes the system to perform multiple consecutive 
Down moves all at once, followed by a poll with more options for the same falling piece if space 
opens up below a bottleneck. Basically, the system performs Down/Stop moves so long as there are no
other valid options.

### Why are the blank spaces filled with medium white squares instead of white space or some other symbol?

Unfortunately, as of 2018 there is no whitespace character that renders with the same width as 
emojis. The white square emojis are the best approximation I've found to having negative space in an 
emoji grid. The small white square doesn't work for this purpose because it takes up four Twitter 
characters, so a 7x11 grid won't fit in a tweet. The large white square fits but it's bulkier and 
I don't think it looks as good as the medium white square.

### When does the emoji theme change?

Right now I'm just sort of changing it semi-manually at the start of each game. I will likely change
that process in the future.

### How many emoji themes are there?

[Eight](https://gitlab.com/JoeSondow/emojitetra/blob/1f5c1d7d963933e9c041b24b2ddb4f50bd2f185b/src/test/java/sondow/tetra/io/EmojiSetTest.java#L26), as of May 2018.

### Does the game restart after Game Over?

Yes.

## Troubleshooting

### Why don't I see a poll sometimes?

If it's game over or showing an intermediary game state where rows are in the process of clearing
then the tweet should not be expected to have a poll; a later tweet should. If a piece is currently
moving then there should be a poll; Twitter sometimes take a while before it renders the poll of a
tweet. Give it a while, or maybe try refreshing the page or app.

### My phone doesn't show some of the characters correctly. What can be done?

Unfortunately it’s impossible to make a game like this that displays properly on all platforms. The
big three emoji environments that people use to read Twitter these days (as of 2018) are Android, 
iOS, and desktop/laptop computers. Blackberry and other environments account for less than 1% of
users, and they don't show many emojis correctly.

### Why do close races occasionally result in a move that is not the final winner of a poll?

In order to make it so that there is always a poll to vote on, the game system reads poll results
in the last minute of the poll and then tweets a new poll right away, often seconds before the 
earlier poll has finished. For most cases this is fine because there is usually a landslide victory
for one option. However, there are uncommon cases where two choices were neck-and-neck and the poll
results had a different winner right up until the last minute. I've added logic to wait for the end
of the poll when it's a close race, but there are still rare instances when Twitter inexplicably
waits a very long time before marking poll results final. When that happens, the bot does its best
to assess the results as they are shown after the poll has been over for a while, and then moves on
with the next game state.

### Does the interval between polls get shorter as the score increases?

No.

### Can you please make it so the interval between polls get shorter as the score increases?

No, I don't plan to do this. I’d rather not make the account seem spammy. Tweet frequency is a delicate 
balancing act. I think a lot of people would avoid following if it accelerated. Also, there are other 
ways I plan on making the game easier and faster overall, and then ways to make it more challenging 
as the score increases, other than speed. Speed isn’t a good challenge vector for this format, in my 
opinion.

### Is there a "Hold" button to put the current piece aside and deal with the "Next" piece now instead?

No.

### What changes are planned for the future?

The [Issue Boards](https://gitlab.com/JoeSondow/emojitetra/boards) page shows all the issues waiting
to be addressed.

## Technical

### Why is this hosted on GitLab instead of GitHub?

GitLab is mostly a functional copy of GitHub, but GitLab provides free private repositories, and
I was put off of GitHub after reading [this story by one of GitHub's former employees](https://where.coraline.codes/blog/my-year-at-github/).
Also I like GitLab's [Issue Boards](https://gitlab.com/JoeSondow/emojitetra/boards) a lot. 
GitHub doesn't have seem to have issue boards, as of 2018. 

### Will a single game go on indefinitely?

Unlikely. The first month of play showed most games ending after a few days, due to some 
combination of bad luck and poor decisions.

### Why are the tweets shown as being posted by Twitter for iPhone?

That's the only way I've found so far to post twitter polls programmatically. Twitter developers
haven't added poll support to the official API yet.

### Where is this system deployed?

This bot is deployed on the [AWS Lambda](https://aws.amazon.com/lambda/) serverless code execution 
service, using the [AWS DynamoDB](https://aws.amazon.com/dynamodb/) database service, outputting to
[Twitter](https://twitter.com/EmojiTetra).

### How was the bot made?

Emoji Tetra includes these technologies:
 
* [Java](http://www.oracle.com/technetwork/java/javase/overview/index.html)
* [Groovy](http://groovy-lang.org/) 
* [Gradle](https://maven.apache.org/)
* [Twitter4J](http://twitter4j.org/)
* [JUnit](https://junit.org)
* [Spock](http://spockframework.org/)
* [System Rules](https://stefanbirkner.github.io/system-rules/)
* [AWS SDK for Java](https://aws.amazon.com/sdk-for-java/)
* [Shadow plugin for Gradle](http://imperceptiblethoughts.com/shadow/)

### Why Java? Why not Ruby/Python/Groovy/Kotlin/Scala/JavaScript/Rust/Go/Assembly?

I like Java. I'm good at it. It has excellent tool support and a large, robust community. I have
more fun building stuff than learning more languages.

I used to code mostly in Groovy and JavaScript, but I now prefer the tool support for Java.

### How are the version numbers chosen?

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/JoeSondow/emojitetra/tags).

### What is the license for this codebase?

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

