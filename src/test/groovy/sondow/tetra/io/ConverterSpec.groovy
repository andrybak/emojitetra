package sondow.tetra.io

import sondow.tetra.game.Game
import sondow.tetra.game.Move
import sondow.tetra.game.Tile
import sondow.tetra.shape.ShapeType
import spock.lang.Specification

import static sondow.tetra.game.Direction.WEST

class ConverterSpec extends Specification {

    def 'should convert simple game string to game with and without previous move'() {

        setup:
        String contents = '{"thm":"FRUIT","thmLen":57,"thr":31,"rows":"9*.7-.7-.I4ZZ","cur":' +
                '{"t":"J","c":3,"r":2,"d":"E"' + prevDb + '},"nx":"Z","sc":100,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🍏◽◽◽　Next\n" +
                "◽◽◽🍏◽◽◽　◽🍎\n" +
                "◽◽🍏🍏◽◽◽　🍎🍎\n" +
                "◽◽◽◽◽◽◽　🍎\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　100\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🍑🍑🍑🍑🍎🍎"

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 100
        game.piece.previousMove == prevMove
        game.nextShape == ShapeType.ZEE

        where:
        prevDb      | prevMove
        ''          | null
        ',"p":"lf"' | Move.LEFT
        ',"p":"ri"' | Move.RIGHT
        ',"p":"lr"' | Move.LEFT_OR_RIGHT
        ',"p":"ro"' | Move.ROTATE
        ',"p":"dn"' | Move.DOWN
        ',"p":"st"' | Move.STOP
        ',"p":"pl"' | Move.PLUMMET
    }

    def 'should convert simple game to database string with and without previous move'() {

        setup:
        Random random = new Random(2)
        Converter converter = new Converter(random)
        Game game = new Game(EmojiSet.ANIMAL, random)
        game.piece = ShapeType.createShape(ShapeType.JAY, game, WEST, 1, 3, prevMove)
        game.nextShape = ShapeType.SQUARE
        game.score = 300
        game.threadLength = 24
        game.tweetId = 656565L
        game.addTile(new Tile(ShapeType.ELL, game), 10, 3)

        when:
        String json = converter.makeJsonFromGame(game)

        then:
        json == '{"sc":300,"cur":{' + prevDb + '"r":1,"c":3,"t":"J","d":"W"},"thm":"ANIMAL",' +
                '"nx":"O","id":656565,"rows":"9*.7-.7-.3L.3","thr":24}'

        where:
        prevMove           | prevDb
        null               | ''
        Move.LEFT          | '"p":"lf",'
        Move.RIGHT         | '"p":"ri",'
        Move.LEFT_OR_RIGHT | '"p":"lr",'
        Move.ROTATE        | '"p":"ro",'
        Move.DOWN          | '"p":"dn",'
        Move.PLUMMET       | '"p":"pl",'
        Move.STOP          | '"p":"st",'
    }
}
