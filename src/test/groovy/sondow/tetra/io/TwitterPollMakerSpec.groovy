package sondow.tetra.io

import spock.lang.Specification
import twitter4j.CardsTwitterImpl
import twitter4j.User
import twitter4j.conf.Configuration

class TwitterPollMakerSpec extends Specification {

    static String longBio = 'I am a bot created by @JoeSondow. Vote on the next move. Not to be ' +
            'confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 13850'
    static String longBio15k = 'I am a bot created by @JoeSondow. Vote on the next move. Not to ' +
            'be confused with any trademarked block-fitting game. #TeamPlummet #TeamRotate. High ' +
            'Score: 15000'

    def "should update bio with new high score"() {
        setup:
        Configuration configuration = Mock()
        CardsTwitterImpl twitter = Mock()
        TwitterPollMaker twitterPollMaker = new TwitterPollMaker(configuration, twitter)
        User user = Mock()

        when:
        twitterPollMaker.updateBioForScore(newScore)

        then:
        1 * configuration.getUser() >> 'Sammy'
        1 * twitter.showUser('Sammy') >> user
        1 * user.getDescription() >> oldDesc
        updates * user.getLocation() >> null
        updates * user.getURL() >> 'gitlab.com/emojitetra'
        updates * user.getScreenName() >> 'Sammy'
        updates * twitter.updateProfile('Sammy', 'gitlab.com/emojitetra', null, newDesc)
        0 * _._

        where:
        newScore | updates | oldDesc                  | newDesc
        230      | 1       | 'Wassup High Score: 100' | 'Wassup High Score: 230'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        90       | 0       | 'Wassup High Score: 100' | 'Wassup High Score: 100'
        1400     | 0       | 'I love you'             | 'I love you'
        15000    | 1       | longBio                  | longBio15k

    }
}
