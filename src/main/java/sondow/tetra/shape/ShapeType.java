package sondow.tetra.shape;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Move;

import static sondow.tetra.shape.Shape.COLUMN_START;
import static sondow.tetra.shape.Shape.ROW_START;

public enum ShapeType {
    ZEE(ShapeZee::new, "Z", ShapeZee::displayTall),
    LINE(ShapeLine::new, "I", ShapeLine::displayTall),
    TEE(ShapeTee::new, "T", ShapeTee::displayTall),
    JAY(ShapeJay::new, "J", ShapeJay::displayTall),
    SQUARE(ShapeSquare::new, "O", ShapeSquare::displayTall, Rotatability.DISABLED),
    ELL(ShapeEll::new, "L", ShapeEll::displayTall),
    ESS(ShapeEss::new, "S", ShapeEss::displayTall);

    private Function<ShapeConfig, Shape> create;

    /**
     * The function that takes a symbol for display and returns a list of strings for displaying
     * the shape in a vertical format.
     */
    private Function<String, List<String>> displayTall;

    /**
     * Used only for database storage.
     */
    private String letterRepresentation;

    /**
     * Some shapes like squares are not changed by rotating so the Rotate option should be omitted.
     */
    private Rotatability rotatability = Rotatability.ENABLED;

    ShapeType(Function<ShapeConfig, Shape> create, String letter,
              Function<String, List<String>> displayTall) {
        this.create = create;
        this.letterRepresentation = letter;
        this.displayTall = displayTall;
    }

    ShapeType(Function<ShapeConfig, Shape> create, String letter,
              Function<String, List<String>> displayTall, Rotatability rotatability) {
        this(create, letter, displayTall);
        this.rotatability = rotatability;
    }

    private static Map<String, ShapeType> REVERSE_LOOKUP = new HashMap<>();

    static {
        ShapeType[] shapeTypes = values();
        for (ShapeType shapeType : shapeTypes) {
            REVERSE_LOOKUP.put(shapeType.letterRepresentation, shapeType);
        }
    }

    public static ShapeType pickAny(Random random) {
        ShapeType[] values = values();
        return values[random.nextInt(values.length)];
    }

    public static Shape createAny(Random random, Game context) {
        return createShapeAtSpawnPoint(pickAny(random), context, Direction.NORTH);
    }

    public static Shape createShape(ShapeType shapeType, Game context, Direction direction, int
            rowIndex, int colIndex, Move previousMove) {
        ShapeConfig config = new ShapeConfig(context, direction, rowIndex, colIndex,
                previousMove);
        Function<ShapeConfig, Shape> createFunc = shapeType.create;
        return createFunc.apply(config);
    }

    public static Shape createShapeAtSpawnPoint(ShapeType type, Game context, Direction direction) {
        return createShape(type, context, direction, ROW_START, COLUMN_START, null);
    }

    public static ShapeType fromLetter(String letter) throws NoSuchShapeException {
        if (isValidLetter(letter)) {
            return REVERSE_LOOKUP.get(letter);
        } else {
            throw new NoSuchShapeException(letter);
        }
    }

    public static boolean isValidLetter(String letter) {
        return REVERSE_LOOKUP.containsKey(letter);
    }

    public String getLetterRepresentation() {
        return letterRepresentation;
    }

    /**
     * Some shapes like squares are not changed by rotating so the Rotate option should be omitted.
     */
    public Rotatability getRotatability() {
        return rotatability;
    }

    public List<String> getTallDisplay(String symbol) {
        return this.displayTall.apply(symbol);
    }
}
