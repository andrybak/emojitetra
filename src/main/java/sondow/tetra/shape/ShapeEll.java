package sondow.tetra.shape;

import java.util.Arrays;
import java.util.List;
import sondow.tetra.game.BadCoordinatesException;

/**
 * <code>ShapeEll</code> is a configuration of <code>Tile</code> objects resembling the letter
 * "J" in shape (see WEST configuration below).
 * <p>
 * The configuration of this piece for each direction appears as follows. The rotation symbol in
 * each diagram indicates the position of the hub square around which the piece rotates. The
 * small white squares are placeholders for layout. They are not part of the tetramino.
 * <p>
 * Note that the hub square of this tetramino is sometimes but not always one of the squares that
 * comprise the shape.
 * <p>
 * <code>
 * <p>
 * NORTH
 * 🐯🔄🐯
 * 🐯️️
 * <p>
 * WEST
 * 🐯
 * 🐯🔄
 * 🐯🐯
 * <p>
 * SOUTH
 * ◽️🔄️🐯
 * 🐯🐯🐯
 * <p>
 * EAST
 * 🐯🐯
 * ◽️🔄
 * ◽️🐯
 * <p>
 * </code>
 *
 * @author @JoeSondow
 */
public class ShapeEll extends Shape {

    /**
     * Creates a new <code>ShapeEll</code> object, also called a piece.
     *
     * @param config the Game object that owns the new piece, and Direction the piece faces
     */
    ShapeEll(ShapeConfig config) {
        super(ShapeType.ELL, config);
    }

    @Override
    public ShapeType getShapeType() {
        return ShapeType.ELL;
    }

    /**
     * Updates the positions of all the tiles in this piece, based on: <br>
     * - the configuration of <code>ShapeEll</code>, <br>
     * - the piece's current rotation (NORTH, EAST, SOUTH, or WEST), <br>
     * - the column index and row index of the hub square for this piece
     *
     * @throws BadCoordinatesException If updating the tile positions causes an illegal situation,
     *                                 such as a tile intersecting another or being off the game
     *                                 grid. This generally results in a "Game Over" condition.
     */
    @Override
    public void updateTiles() throws BadCoordinatesException {
        int col = columnIndex, row = rowIndex;
        switch (direction) {
            case NORTH: {
                // Configuration appears as follows (@ is hub square):
                //
                // +@+
                // +
                setTilePosition(1, col, row);
                setTilePosition(2, col + 1, row);
                setTilePosition(3, col - 1, row);
                setTilePosition(4, col - 1, row + 1);
                break;
            }
            case WEST: {
                // Configuration appears as follows (@ is hub square):
                //
                // +
                // +@
                // ++
                setTilePosition(1, col - 1, row);
                setTilePosition(2, col - 1, row - 1);
                setTilePosition(3, col - 1, row + 1);
                setTilePosition(4, col, row + 1);
                break;
            }
            case SOUTH: {
                // Configuration appears as follows (@ is hub square):
                //
                //  @+
                // +++
                setTilePosition(1, col, row + 1);
                setTilePosition(2, col - 1, row + 1);
                setTilePosition(3, col + 1, row + 1);
                setTilePosition(4, col + 1, row);
                break;
            }
            case EAST: {
                // Configuration appears as follows (@ is hub square):
                //
                // ++
                //  @
                //  +
                setTilePosition(1, col, row);
                setTilePosition(2, col, row + 1);
                setTilePosition(3, col, row - 1);
                setTilePosition(4, col - 1, row - 1);
                break;
            }
        }
    }

    static List<String> displayTall(String symbol) {
        return Arrays.asList(symbol, symbol, symbol + symbol);
    }
}
