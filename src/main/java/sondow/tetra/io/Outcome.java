package sondow.tetra.io;

import twitter4j.Status;

/**
 * The outcome of executing the whole program, mostly for the purposes of logging.
 */
public class Outcome {

    private Status tweet;
    private long retryDelaySeconds;

    public Outcome() {
    }

    public Status getTweet() {
        return tweet;
    }

    public Outcome setTweet(Status tweet) {
        this.tweet = tweet;
        return this;
    }

    @Override
    public String toString() {
        return tweet.toString();
    }

    public void setRetryDelaySeconds(long retryDelaySeconds) {
        this.retryDelaySeconds = retryDelaySeconds;
    }

    public long getRetryDelaySeconds() {
        return retryDelaySeconds;
    }
}
