package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;
import sondow.tetra.io.EmojiSet;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;

import static sondow.tetra.game.Move.DOWN;
import static sondow.tetra.game.Move.LEFT;
import static sondow.tetra.game.Move.LEFTORRIGHT_ROTATE_DOWN;
import static sondow.tetra.game.Move.LEFT_OR_RIGHT;
import static sondow.tetra.game.Move.LEFT_RIGHT_ROTATE_DOWN;
import static sondow.tetra.game.Move.PLUMMET;
import static sondow.tetra.game.Move.RIGHT;
import static sondow.tetra.game.Move.ROTATE;
import static sondow.tetra.game.Move.STOP;
import static sondow.tetra.shape.Rotatability.DISABLED;

/**
 * The game grid and current falling piece and related functionality.
 *
 * @author @JoeSondow
 */
public class Game {

    private static final String ideographicSpace = "　";

    /**
     * Number rows in the grid of tile spaces
     */
    private final int rowCnt = 11;

    /**
     * Number columns in the grid of tile spaces
     */
    private final int colCnt = 7;

    /**
     * The grid of spaces to hold tiles
     */
    private Tile grid[][] = new Tile[rowCnt][colCnt];

    private EmojiSet emojiSet;

    /**
     * Set of rows that have just been altered by the landing of a piece, prior to row deletions.
     * These rows should be checked for completion so as to delete any rows that have just been
     * filled.
     */
    private TreeSet<Integer> alteredRows = new TreeSet<>();

    /**
     * Total number of rows that have been deleted since the current game started
     */
    private int rowsDeleted = 0;

    /**
     * Flag for Game Over status
     */
    private boolean gameOver = false;

    private int score;

    private ShapeType nextShape;

    /**
     * The currently falling piece
     */
    private Shape piece;

    private Random random;
    private Long tweetId;

    /**
     * Number of tweets since new twitter thread started.
     */
    private int threadLength;

    /**
     * Instantiates a game grid.
     */
    public Game(EmojiSet emojiSet, Random random) {
        this.emojiSet = emojiSet;
        this.random = random;
    }

    /**
     * Checks to see whether a specified location in the grid contains a tile or not. Does not apply
     * to falling tiles, only tiles that are already part of the non-moving pile of tiles in the
     * grid object.
     *
     * @param row the grid row of the square to check
     * @param col the grid column of the square to check
     * @return <code>true</code> if there is a tile at that square, <code>false</code> otherwise
     */
    boolean hasTileAt(int row, int col) {
        return grid[row][col] != null;
    }

    /**
     * Places a tile into the grid so that the tile is part of the non-moving pile of tiles, rather
     * than part of the currently falling piece.
     *
     * @param myTile the tile to be added to the game grid
     * @param row    the grid row where the tile will be placed
     * @param col    the grid column where the tile will be placed
     */
    public void addTile(Tile myTile, int row, int col) {
        grid[row][col] = myTile;

        // Remember that this row has been altered but not checked for deletion
        alteredRows.add(row);
    }

    /**
     * Returns the tile object at the specified location in the grid, or null if there is no tile
     * at that location. Does not access tiles of falling pieces, only of non-moving tiles that are
     * already in the grid object.
     *
     * @param row the grid row of the square we want to examine
     * @param col the grid column of the square we want to examine
     * @return the tile object at the specified location, or null if there is no tile there
     */
    private Tile getTile(int row, int col) {
        return grid[row][col];
    }

    /**
     * Deletes a row from the grid (probably because the row was recently filled with tiles) and
     * drops down everything above that row, simulating gravity
     *
     * @param row the grid row index to delete
     */
    private void deleteRow(int row) {
        // Move each higher row (lesser value) to lower position (greater value)
        //noinspection ManualArrayCopy
        for (int r = row; r > 0; r--) {
            grid[r] = grid[r - 1];
            // System.out.println("row " + (r-1) + " moved to row " + (r));
        }
        grid[0] = new Tile[colCnt]; // Make top row empty and distinct
    }

    /**
     * Looks through all the rows altered by a newly fallen piece, and deletes any altered rows that
     * have become full.
     *
     * @return number of filled rows found and deleted
     */
    private int deleteFilledRows() {
        int rowsDeleted = 0;
        // Starting with the highest altered row (lowest row number)
        // check each altered to see if it's filled
        for (Integer alteredRow : alteredRows) {
            // Check the next eligible row
            int row = alteredRow;

            // Check each space in row
            boolean foundEmpty = false;
            for (int col = 0; col < colCnt && !foundEmpty; col++) {
                if (!hasTileAt(row, col)) {
                    foundEmpty = true; // Stop checking this row
                }
            }
            // If all spaces in row are filled
            if (!foundEmpty) {
                deleteRow(row);
                rowsDeleted++;
            }
        }
        // If the entire grid gets 100% cleared, players get a bonus.
        GridOccupancy gridOccupancy = GridOccupancy.VACANT;
        for (int row = 0; row < rowCnt && gridOccupancy == GridOccupancy.VACANT; row++) {
            for (int col = 0; col < colCnt && gridOccupancy == GridOccupancy.VACANT; col++) {
                if (hasTileAt(row, col)) {
                    gridOccupancy = GridOccupancy.OCCUPIED;
                }
            }
        }

        scoreDeletedRows(rowsDeleted, gridOccupancy);
        alteredRows.clear(); // Empty all values from set
        return rowsDeleted;
    }

    private static Map<Integer, Integer> ROWS_TO_POINTS_PER_ROW = new HashMap<>();

    static {
        ROWS_TO_POINTS_PER_ROW.put(0, 0);
        ROWS_TO_POINTS_PER_ROW.put(1, 100);
        ROWS_TO_POINTS_PER_ROW.put(2, 125); // 100+25
        ROWS_TO_POINTS_PER_ROW.put(3, 175); // 100+25+50
        ROWS_TO_POINTS_PER_ROW.put(4, 250); // 100+25+50+75
    }

    void scoreDeletedRows(int rowsDeleted, GridOccupancy gridOccupancy) {
        if (rowsDeleted < 0) {
            throw new RuntimeException("Can't delete " + rowsDeleted + " rows");
        }
        Integer pointsPerRow = ROWS_TO_POINTS_PER_ROW.get(rowsDeleted);
        if (pointsPerRow == null) {
            pointsPerRow = 250;
        }
        int pointsToAdd = rowsDeleted * pointsPerRow;
        if (rowsDeleted >= 1 && gridOccupancy == GridOccupancy.VACANT) {
            pointsToAdd += 1000;
        }
        setScore(getScore() + pointsToAdd);
    }

    /**
     * Creates a new randomly chosen piece at its default location
     */
    void spawnPiece() {
        try {
            piece = ShapeType.createShapeAtSpawnPoint(getNextShape(), this, Direction.NORTH);
            setNextShape(ShapeType.pickAny(random));
        } catch (BadCoordinatesException e) {
            System.out.println("Failed to spawn a " + nextShape + " at " + e.getMessage());
            endGame();
            throw new GameOverException();
        }
    }

    /**
     * Renders the current contents of the game grid.
     */
    @Override
    public String toString() {
        Grid grid = constructGridWithCurrentPiece();
        putStaticTilesInGrid(grid);
        StringBuilder builder = new StringBuilder();
        ShapeType nextShape = getNextShape();
        String nextSymbol = emojiSet.symbolFor(nextShape);
        List<String> tallDisplay = nextShape.getTallDisplay(nextSymbol);
        List<String> rowsAsStrings = grid.getRowsAsStrings();
        String b = chooseBorderEmoji();
        for (int i = 0; i < rowsAsStrings.size(); i++) {
            if (i > 0) {
                builder.append("\n");
            }
            String row = buildGridRow(rowsAsStrings, b, i);
            builder.append(row);
            StringBuilder rightHandSideBuilder = buildRightHandSideOfRow(tallDisplay, i);
            builder.append(rightHandSideBuilder);
        }
        return builder.toString();
    }

    private StringBuilder buildRightHandSideOfRow(List<String> tallDisplay, int i) {
        StringBuilder rightHandSideBuilder = new StringBuilder();
        if (i == 1) {
            rightHandSideBuilder.append(ideographicSpace).append("Next");
        } else if (i == 2) {
            rightHandSideBuilder.append(ideographicSpace).append(tallDisplay.get(0));
        } else if (i == 3) {
            rightHandSideBuilder.append(ideographicSpace).append(tallDisplay.get(1));
        } else if (i == 4 && tallDisplay.size() >= 3) {
            rightHandSideBuilder.append(ideographicSpace).append(tallDisplay.get(2));
        } else if (i == 5 && tallDisplay.size() >= 4) {
            rightHandSideBuilder.append(ideographicSpace).append(tallDisplay.get(3));
        } else if (i == 7) {
            rightHandSideBuilder.append(ideographicSpace).append("Score");
        } else if (i == 8) {
            rightHandSideBuilder.append(ideographicSpace).append(getScore());
        }
        return rightHandSideBuilder;
    }

    private String buildGridRow(List<String> rowsAsStrings, String borderEmoji, int index) {
        String row = rowsAsStrings.get(index);
        if (isGameOver()) {
            if (index == 3 || index == 6) {
                row = borderEmoji + borderEmoji + borderEmoji + borderEmoji + borderEmoji +
                        borderEmoji + borderEmoji;
            } else if (index == 4) {
                row = borderEmoji + "\uD83C\uDDEC \uD83C\uDDE6 \uD83C\uDDF2 \uD83C\uDDEA" +
                        borderEmoji + borderEmoji;
            } else if (index == 5) {
                row = borderEmoji + borderEmoji +
                        "\uD83C\uDDF4 \uD83C\uDDFB \uD83C\uDDEA \uD83C\uDDF7" + borderEmoji;
            }
        }
        return row;
    }

    private String chooseBorderEmoji() {
        List<String> borderEmojis = Arrays.asList("✨", "☠️", "⚰️", "🎵", "🎶", "⭐️", "💫",
                "❄️️", "☁️", "⚡️", "💥", "🔥", "☀️", "🌈", "🌀", "🔔", "♦️");
        return borderEmojis.get(random.nextInt(borderEmojis.size()));
    }

    private void putStaticTilesInGrid(Grid grid) {
        for (int row = 0; row < rowCnt; row++) {
            for (int col = 0; col < colCnt; col++) {
                if (hasTileAt(row, col)) {
                    Tile tile = getTile(row, col);
                    ShapeType shapeType = tile.getShapeType();
                    String symbol = emojiSet.symbolFor(shapeType);
                    grid.put(row, col, symbol);
                }
            }
        }
    }

    private Grid constructGridWithCurrentPiece() {
        Grid grid = new Grid(rowCnt, colCnt, EmojiSet.blank);

        if (piece != null && piece.isFalling()) {
            String currentSymbol = emojiSet.symbolFor(piece.getShapeType());
            // symbol should be ⬛️ when down is not a legal move but stop is.
            if (!canMove(Move.DOWN)) {
                currentSymbol = "⬛";
            }
            if (piece.getTileCol(1) >= 0) {
                grid.put(piece.getTileRow(1), piece.getTileCol(1), currentSymbol);
            }
            if (piece.getTileCol(2) >= 0) {
                grid.put(piece.getTileRow(2), piece.getTileCol(2), currentSymbol);
            }
            if (piece.getTileCol(3) >= 0) {
                grid.put(piece.getTileRow(3), piece.getTileCol(3), currentSymbol);
            }
            if (piece.getTileCol(4) >= 0) {
                grid.put(piece.getTileRow(4), piece.getTileCol(4), currentSymbol);
            }
        }
        return grid;
    }

    /**
     * For the case of STOP, the rules are totally different. You don't try to stop the piece.
     * That would make no sense. Instead, you try to go DOWN, and if you cannot go DOWN, then
     * you can STOP. It's always possible to either go DOWN or STOP, never both, never neither,
     * unless it's Game Over.
     *
     * @param move the move to consider
     * @return true if the piece can legally perform the specified move including implicit
     * precursor moves such as DOWN before ROTATE, false otherwise
     * @throws RuntimeException if specified move is LEFT_OR_RIGHT or PLUMMET
     */
    private boolean canMove(Move move) {
        boolean ableToMove = false;
        if (move == ROTATE) {
            if (canDoSingleMove(ROTATE)) {
                ableToMove = true;
            } else if (canDoSingleMove(DOWN)) {
                Shape unalteredPiece = piece.copy();
                doSingleMove(DOWN);
                if (canDoSingleMove(ROTATE)) {
                    ableToMove = true;
                } else if (canDoSingleMove(DOWN)) {
                    doSingleMove(DOWN);
                    if (canDoSingleMove(ROTATE)) {
                        ableToMove = true;
                    }
                }
                setPiece(unalteredPiece);
            }
        } else {
            ableToMove = canDoSingleMove(move);
        }
        return ableToMove;
    }

    /**
     * Checks whether the current piece can legally perform the specified move in one step
     * without any precursor implicit moves. In the case of ROTATE, this method will return false if
     * the piece is up against the top of the grid, because there isn't room above the piece for its
     * tiles to rotate into. This is in contrast to the canMove method which checks whether a
     * ROTATE is legal if the game can first perform one or more precursor DOWN moves before the
     * ROTATE becomes legal.
     *
     * @param move the move to consider
     * @return true if the piece can legally perform the specified move in one step, false otherwise
     * @throws RuntimeException if specified move is LEFT_OR_RIGHT or PLUMMET
     */
    private boolean canDoSingleMove(Move move) {
        if (move == LEFT_OR_RIGHT || move == PLUMMET) {
            throw new RuntimeException("'" + move + "'  isn't supposed to be passed into canMove " +
                    "ever because it would have undesirable side effects. Tweet ID: " +
                    getTweetId() + ". Game: " + this);
        }
        if (move == STOP) {
            return !canMove(DOWN);
        }
        if (move == ROTATE && piece.getShapeType().getRotatability().equals(DISABLED)) {
            return false; // IF SQUARE THAN NO ROTATE YO
        }
        // Clone piece. Move clone, check for illegal state, remove clone, restore original piece.
        Shape unalteredPiece = piece.copy();
        boolean ableToMove;
        try {
            doSingleMove(move);
            ableToMove = true;
        } catch (BadCoordinatesException e) {
            ableToMove = false;
        }
        setPiece(unalteredPiece);
        return ableToMove;
    }

    void doMove(Move move) {
        if (move == ROTATE) {
            if (!canDoSingleMove(ROTATE) && canDoSingleMove(DOWN)) {
                doSingleMove(DOWN);
                if (!canDoSingleMove(ROTATE) && canDoSingleMove(DOWN)) {
                    doSingleMove(DOWN);
                }
            }
            doSingleMove(ROTATE);
        } else if (move == DOWN) {
            // Move the piece down a number of times equal to half of the count of blank rows
            // below the current piece.
            int blankRows = countBlankRowsBelowPiece();
            int rowsToDescend = halfRoundedUpMinimumOne(blankRows);
            for (int i = 0; i < rowsToDescend; i++) {
                doSingleMove(DOWN);
            }
        } else {
            doSingleMove(move);
        }
        doForcedMoves();
    }

    private void doSingleMove(Move move) {
        piece.setPreviousMove(move);
        // Throw a BadCoordinatesException if doing the move is currently blocked.
        switch (move) {
            case LEFT:
                piece.left();
                break;
            case RIGHT:
                piece.right();
                break;
            case LEFT_OR_RIGHT:
                // All that matters is that the previousMove has been set to LEFT_OR_RIGHT
                break;
            case ROTATE:
                piece.rotate();
                break;
            case DOWN:
                piece.down();
                break;
            case STOP:
                stopPieceDeleteFilledRowsAndSpawnPiece();
                break;
            case PLUMMET:
                // Go down until you no longer can go down, then stop.
                while (canMove(Move.DOWN)) {
                    piece.down();
                }
                stopPieceDeleteFilledRowsAndSpawnPiece();
                break;
        }
    }

    private void stopPieceDeleteFilledRowsAndSpawnPiece() {
        piece.stop();
        // This is only okay because when canMove calls doSingleMove to do a dry run on a
        // temporary piece, STOP is never ever passed in to doSingleMove by canMove.
        // The only time STOP is passed in to doSingleMove is when the player really does execute
        // a STOP move.
        deleteFilledRows();
        spawnPiece();
    }

    int halfRoundedUpMinimumOne(int blankRows) {
        return Math.max(1, (int) Math.ceil(blankRows / 2d));
    }

    int countBlankRowsBelowPiece() {
        int pieceBottomRowIndex = piece.getBottomTileRowIndex();
        // Iterate through the rows of the grid below the piece, and count how many of those rows
        // have zero tiles in them.

        int countOfEmptyRowsBelowPiece = 0;
        for (int i = pieceBottomRowIndex + 1; i < grid.length; i++) {
            Tile[] row = grid[i];
            // Is this row empty?
            boolean isRowOccupied = false;
            for (Tile tile : row) {
                if (tile != null) {
                    isRowOccupied = true;
                }
            }
            if (!isRowOccupied) {
                countOfEmptyRowsBelowPiece++;
            }
        }
        return countOfEmptyRowsBelowPiece;
    }

    private final List<Move> DOWN_PLUMMET = Arrays.asList(Move.DOWN, Move.PLUMMET);

    /**
     * Checks to see if there are meaningful choices for players to make. Performs "Down" moves
     * until either the piece has other options or until the piece can do nothing but "Stop" and
     * spawn the next piece.
     */
    private void doForcedMoves() {
        List<Move> moves = listLegalMoves();
        while (moves.size() == 1 || moves.equals(DOWN_PLUMMET)) {
            Move nextMove = moves.get(0);
            doSingleMove(nextMove);
            moves = listLegalMoves();
        }
    }

    int getColumnCount() {
        return colCnt;
    }

    int getRowCount() {
        return rowCnt;
    }

    boolean isGameOver() {
        return gameOver;
    }

    void endGame() {
        this.gameOver = true;
    }

    public int getScore() {
        return score;
    }

    public Game setScore(int score) {
        this.score = score;
        return this;
    }

    public ShapeType getNextShape() {
        if (nextShape == null) {
            nextShape = ShapeType.pickAny(random);
        }
        return nextShape;
    }

    public Game setNextShape(ShapeType nextShape) {
        this.nextShape = nextShape;
        return this;
    }

    public EmojiSet getEmojiSet() {
        return emojiSet;
    }

    public Shape getPiece() {
        return piece;
    }

    public Game setPiece(Shape piece) {
        this.piece = piece;
        return this;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public Long getTweetId() {
        return tweetId;
    }

    public List<List<Tile>> getRows() {
        ArrayList<List<Tile>> rows = new ArrayList<>();

        for (Tile[] rowArray : grid) {
            List<Tile> rowList = Arrays.asList(rowArray);
            rows.add(rowList);
        }
        return rows;
    }

    List<Move> listLegalMoves() {
        List<Move> legalMoves = new ArrayList<>();
        Move prev = getPiece().getPreviousMove();
        if (prev == LEFT_OR_RIGHT) {
            legalMoves.add(LEFT);
            legalMoves.add(RIGHT);
        } else {
            // Collapse Left/Right into one if all 4 poll slots are taken, to make room for Plummet
            if (prev == DOWN && LEFT_RIGHT_ROTATE_DOWN.stream().allMatch(this::canMove)) {
                legalMoves.addAll(LEFTORRIGHT_ROTATE_DOWN);
            } else {
                // Try 4 possible standard moves as dry runs, and see whether they are legal.
                for (Move move : LEFT_RIGHT_ROTATE_DOWN) {
                    if (canMove(move)) {
                        legalMoves.add(move);
                    }
                }
            }
            addPlummetOrStopIfAppropriate(legalMoves);
        }
        Collections.sort(legalMoves);
        return legalMoves;
    }

    private void addPlummetOrStopIfAppropriate(List<Move> legalMoves) {
        if (legalMoves.contains(DOWN)) {
            if (legalMoves.size() < 4) {
                legalMoves.add(PLUMMET);
            }
        } else {
            legalMoves.add(STOP); // You can only Stop when you cannot go Down
        }
    }

    public Game setThreadLength(int threadLength) {
        this.threadLength = threadLength;
        return this;
    }

    public int getThreadLength() {
        return threadLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Game game = (Game) o;

        if (rowsDeleted != game.rowsDeleted) {
            return false;
        }
        if (gameOver != game.gameOver) {
            return false;
        }
        if (score != game.score) {
            return false;
        }
        if (threadLength != game.threadLength) {
            return false;
        }
        if (!Arrays.deepEquals(grid, game.grid)) {
            return false;
        }
        if (emojiSet != game.emojiSet) {
            return false;
        }
        if (alteredRows != null ? !alteredRows.equals(game.alteredRows) : game.alteredRows !=
                null) {
            return false;
        }
        if (nextShape != game.nextShape) {
            return false;
        }
        //noinspection SimplifiableIfStatement
        if (piece != null ? !piece.equals(game.piece) : game.piece != null) {
            return false;
        }
        return tweetId != null ? tweetId.equals(game.tweetId) : game.tweetId == null;
    }

    @Override
    public int hashCode() {
        int result = Arrays.deepHashCode(grid);
        result = 31 * result + (emojiSet != null ? emojiSet.hashCode() : 0);
        result = 31 * result + (alteredRows != null ? alteredRows.hashCode() : 0);
        result = 31 * result + rowsDeleted;
        result = 31 * result + (gameOver ? 1 : 0);
        result = 31 * result + score;
        result = 31 * result + (nextShape != null ? nextShape.hashCode() : 0);
        result = 31 * result + (piece != null ? piece.hashCode() : 0);
        result = 31 * result + (tweetId != null ? tweetId.hashCode() : 0);
        result = 31 * result + threadLength;
        return result;
    }
}
