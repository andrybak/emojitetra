package sondow.tetra.game;

/**
 * This exception gets thrown when requesting a Direction for a character or string that does not
 * have a matching known Direction.
 *
 * @author @JoeSondow
 */
class NoSuchDirectionException extends RuntimeException {

    /**
     * Creates a new <code>NoSuchDirectionException</code> for a specified non-direction string.
     *
     * @param s the string for which a Direction was requested
     */
    NoSuchDirectionException(String s) {
        super("No Direction for " + s);
    }
}
