package sondow.tetra.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Move {
    LEFT("⬅️ Left", "lf"),
    RIGHT("➡️ Right", "ri"),
    LEFT_OR_RIGHT("↔️ Left or Right", "lr"),
    ROTATE("🔄 Rotate", "ro"),
    DOWN("⬇️ Down", "dn"),
    STOP("⬇️ Stop", "st"),
    PLUMMET("⏬ Plummet", "pl"),;

    private final String emojiAndTitleCase;
    private final String abbreviation;

    static List<Move> LEFT_RIGHT_ROTATE_DOWN = Arrays.asList(LEFT, RIGHT, ROTATE, DOWN);
    static List<Move> LEFTORRIGHT_ROTATE_DOWN = Arrays.asList(LEFT_OR_RIGHT, ROTATE, DOWN);

    Move(String emoji, String abbreviation) {
        this.emojiAndTitleCase = emoji;
        this.abbreviation = abbreviation;
    }

    String getEmojiAndTitleCase() {
        return emojiAndTitleCase;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    private static Map<String, Move> EMOJI_AND_TITLE_CASE_TO_MOVE = new HashMap<>();

    private static Map<String, Move> ABBREVIATION_TO_MOVE = new HashMap<>();

    static {
        for (Move move : Move.values()) {
            ABBREVIATION_TO_MOVE.put(move.getAbbreviation(), move);
        }

        for (Move move : Move.values()) {
            EMOJI_AND_TITLE_CASE_TO_MOVE.put(move.getEmojiAndTitleCase(), move);
        }
    }

    /**
     * Tries to find a matching Move for a given emoji and title case string.
     *
     * @param emojiAndTitleCase the string to look up
     * @return the matching Move
     */
    public static Move fromEmojiAndTitleCase(String emojiAndTitleCase) {
        return EMOJI_AND_TITLE_CASE_TO_MOVE.get(emojiAndTitleCase);
    }

    /**
     * Tries to find a matching Move for a given abbreviation string, probably from database.
     *
     * @param abbreviation the abbreviation to look up
     * @return the matching Move
     */
    public static Move fromAbbreviation(String abbreviation) {
        return ABBREVIATION_TO_MOVE.get(abbreviation);
    }

    /**
     * Poll choice strings for moves show an emojiAndTitleCase, followed by a space, followed by
     * the title case
     * version of the move name.
     *
     * @param moves the moves to convert
     * @return the list of string representations of the moves, with emojis included
     */
    public static List<String> toPollChoices(List<Move> moves) {
        List<String> choices = new ArrayList<>();
        for (Move move : moves) {
            choices.add(move.getEmojiAndTitleCase());
        }
        return choices;
    }
}
